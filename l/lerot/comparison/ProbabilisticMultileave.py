# This file is part of Lerot.
#
# Lerot is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Lerot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Lerot.  If not, see <http://www.gnu.org/licenses/>.

# KH, 2012/06/19

import argparse

from numpy import asarray, where, ndarray
from random import randint

from .AbstractInterleavedComparison import AbstractInterleavedComparison
from ..utils import split_arg_str


class ProbabilisticMultileave(AbstractInterleavedComparison):
    """Probabilistic interleaving, marginalizes over assignments"""

    def __init__(self, arg_str=None):
        if arg_str:
            parser = argparse.ArgumentParser(description="Parse arguments for "
                "interleaving method.", prog=self.__class__.__name__)
            parser.add_argument("-a", "--aggregate", choices=["expectation",
                "log-likelihood-ratio", "likelihood-ratio", "log-ratio",
                "binary"])
            parser.add_argument("-d", "--det_interleave", type=bool,
                help="If true, use deterministic interleaving, regardless "
                "of the ranker type used for comparison.")
            parser.add_argument("-t", "--compare_td", type=bool,
                help="If true, compare rankers using observed assignments "
                "instead of marginalizing over possible assignments.")
            args = vars(parser.parse_known_args(split_arg_str(arg_str))[0])
            if "aggregate" in args and args["aggregate"]:
                self.aggregate = args["aggregate"]
            if "det_interleave" in args and args["det_interleave"]:
                self.det_interleave = True
            if "compare_td" in args and args["compare_td"]:
                self.compare_td = True
        if not hasattr(self, "aggregate") or not self.aggregate:
            self.aggregate = "expectation"
        if not hasattr(self, "det_interleave"):
            self.det_interleave = False
        if not hasattr(self, "compare_td"):
            self.compare_td = False

    def interleave(self, r1, r2, query, length):
        self.multileave([r1, r2], query, length)

    def multileave(self, lr, query, length):
        """
        Chooses the ranker to use from a evenly distributed random distribution
        :param lr: the list of rankers
        :param query: the query
        :param length: the cutoff length
        :return: tuple with:  (array of picked documents from different rankers,
                                    (list from which rankers the corresponding document is picked,
                                     the list of rankers)

        """
        tau = 3

        docdocranks = {}
        for r in lr:
            r.init_ranking(query)
            docdocranks[lr.index(r)] = {str(doc.docid) : i for i, doc in enumerate(r.docids)}
            r.document_count()
            length = min(r.document_count(), length)

        # start with empty document list
        l = []
        # random bits indicate which r to use at each rank
        a = asarray([randint(0, len(lr)-1) for _ in range(length)])
        for next_a in a:
            # flip coin - which r contributes doc (pre-computed in a)
            select = lr[next_a]

            probs = self.softmax(r.docids, docdocranks[next_a], tau)
            select.probs = probs
            # draw doc
            pick = select.next()
            l.append(pick)
            # let other ranker know that we removed this document
            for r in lr:
                if r != select:
                    try:
                        r.rm_document(pick)
                    except:
                        pass
        return (asarray(l), (a, lr))


    def infer_outcome(self, l, a, c, query):
        assignment_sample_size = 10000        
        tau = 3
        (td_a, rankers) = a        
        
        # comparison with marginalization
        # are there any clicks? (otherwise it's a tie)
        click_ids = ndarray.tolist(where(asarray(c) == 1))[0]
        if not len(click_ids):  # no clicks, will be a tie
            return [0]*len(rankers)
        
        # Sample assignents, to approximate real values while saving computation
        samples = self._sampleAssignments(len(rankers), assignment_sample_size, len(l))
        
        # Results for all rankers start at 0
        scores = [0.0]*len(rankers)

        softmax_denominators = []
        "############ Note: Pretty blindly copied from Sebastian, ## can be removed if it's used correctly here."
        docdocranks = {}
        for r in rankers:
            docdocranks[rankers.index(r)] = {doc.docid: i for i, doc in enumerate(r.docids)}
            softmax_denominators.append(sum([1.0 / i**tau for i in range(r.docids)]))
            probs = self.softmax(r.docids, docdocranks, tau)

        "###########"
        "# Calculate the sum over assignments of P(l | a, q) TODO check if this makes sense"
        sum_p_l_aq = 0.0
        for assignment in samples:
            p_l_aq = 1.0
            temp_soft_den = softmax_denominators[0:]
            for i in range(len(l)):
                doc_rank_in_r = docdocranks[assignment[i]][l[i]]
                temp = 1.0 / (doc_rank_in_r ** tau)
                p_l_aq *= temp / temp_soft_den[assignment[i]]
                temp_soft_den[assignment[i]] -= temp
            sum_p_l_aq += p_l_aq   

        p_assignment = pow(1.0 / len(rankers), len(l))
        for assignment in samples:
            # Count the clicks per ranker under assignment so credit can be given.
            click_counts = [0]*len(rankers)
            for click_i in click_ids:
                click_counts[assignment[click_i]] += 1

            #(re)calculate P(l | a, q)#TODO Check if this makes sense
            p_l_aq = 1.0
            for i in range(len(l)):
                doc_rank_in_r = docdocranks[assignment[i]][l[i]]
                temp = 1.0 / (doc_rank_in_r ** tau)
                p_l_aq *= temp / temp_soft_den[assignment[i]]
                temp_soft_den[assignment[i]] -= temp
            
            # Give credit to all rankers getting the most clicks (ties allowed)            
            most_clicks = max(click_counts)
            for i in range(click_counts):
                if click_counts[i] == most_clicks:
                    scores[i] += ((p_l_aq * p_assignment) / sum_p_l_aq)
        return scores
            
    
    # TODO sample more efficiently and remove possibility of duplicates.
    def _sampleAssignments(self, number_of_rankers, sample_size, sample_length):
        samples = []
        for s in sample_size:
            sample = []
            for r in sample_length:
                 sample.append(randint(0, number_of_rankers - 1))
            samples.append(sample)
        return samples


    def softmax(self, documents, docranks,  tau):
        val = 0
        for d in documents:
            print docranks[str(d.docid)]+1
            val += 1.0/(docranks[str(d.docid)]+1)**tau
        print "========================================================"
        return [(1.0/(docranks[str(d.docid)]+1)**tau) / float(val) for d in documents]


    def get_probability_of_list(self, result_list, context, query):
        # P(l) = \prod_{doc in result_list} 1/2 P_1(doc) + 1/2 P_2(doc)
        p_l = 1.0
        (_, r1, r2) = context
        r1.init_ranking(query)
        r2.init_ranking(query)
        for _, doc in enumerate(result_list):
            p_r1 = r1.get_document_probability(doc)
            p_r2 = r2.get_document_probability(doc)
            r1.rm_document(doc)
            r2.rm_document(doc)
            p_l *= 0.5 * (p_r1 + p_r2)
        return p_l


class SimpleBinaryTree:
    """tree that keeps track of outcome, probability of arriving at this
    outcome"""
    parent, left, right, prob, outcome = None, None, None, 0.0, 0

    def __init__(self, parent, prob, outcome):
        self.parent = parent
        self.prob = prob
        self.outcome = outcome
